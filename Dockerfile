FROM elixir:1.9.2-alpine as builder


WORKDIR /app

RUN mix do local.hex --force, local.rebar --force

COPY config/ /app/config/
COPY mix.exs /app/
COPY mix.* /app/

COPY apps/asolvi/mix.exs /app/apps/asolvi/
COPY apps/asolvi_web/mix.exs /app/apps/asolvi_web/

ENV SECRET_KEY_BASE=xAeoxYXYaENMaF2lsTCbcM3fnfRUVZEz2MZoa5gxI52EVKE003xt3RmuDcAx682S
RUN mix do deps.get --only prod, deps.compile

COPY . /app/


WORKDIR /app/apps/asolvi_web
RUN MIX_ENV=prod mix compile
RUN apk add --update nodejs nodejs-npm

RUN npm install --prefix ./assets
RUN npm run deploy --prefix ./assets
RUN MIX_ENV=prod mix phx.digest

WORKDIR /app/
RUN MIX_ENV=prod mix distillery.release

FROM alpine:latest

RUN apk upgrade --no-cache && \
    apk add --no-cache bash openssl

EXPOSE 4000

ENV PORT=4000 \
    MIX_ENV=prod \
    REPLACE_OS_VARS=true \
    SHELL=/bin/bash

WORKDIR /app/

COPY --from=builder /app/_build/prod/rel/asolvi_umbrella/releases/0.1.0/asolvi_umbrella.tar.gz .
RUN tar xzf asolvi_umbrella.tar.gz && rm -f asolvi_umbrella.tar.gz

RUN chown -R root ./releases

USER root

COPY boot.sh boot.sh

ENTRYPOINT ["/app/boot.sh"]

