# Asolvi assignment

## Requirements

  - Erlang 22.1.4
  - Elixir 1.9.2

## Setup

```bash
mix deps.get
mix compile
npm install --prefix apps/asolvi_web/assets/
mix phx.server
```

## Running Tests

```bash
MIX_ENV=test mix test
```


## App architecture

CRUD application for customers

  - `Asolvi.CustomerStateWorker` The app will starts this process(`Agent`) to keep track of all customers. create/read/update/delete
  - `Asolvi.WorkerSupervisor` this supervisor is responsible to add dynamic processes for update and delete individual customer. this is done with help of `Registry`.


```elixir
    children = [
      worker(Asolvi.CustomerStateWorker, []), # Maintain list of customers
      { Asolvi.WorkerSupervisor, [] }, # Dynamically create unique process per customer
      { Registry, [keys: :unique, name: :customer_registry] }
    ]
```
