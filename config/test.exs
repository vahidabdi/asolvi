use Mix.Config

config :asolvi,
  data_path: Path.join(Path.dirname(__DIR__), "priv/test_data")

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :asolvi_web, AsolviWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
