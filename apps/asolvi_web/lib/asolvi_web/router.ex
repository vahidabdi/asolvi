defmodule AsolviWeb.Router do
  use AsolviWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", AsolviWeb do
    pipe_through :browser

    resources "/", ItemController
  end

  # Other scopes may use custom stacks.
  # scope "/api", AsolviWeb do
  #   pipe_through :api
  # end
end
