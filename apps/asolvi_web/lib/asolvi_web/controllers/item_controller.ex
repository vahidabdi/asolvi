defmodule AsolviWeb.ItemController do
  use AsolviWeb, :controller

  alias Asolvi.Customer
  alias Asolvi.CustomerService

  def index(conn, _params) do
    customers = Asolvi.CustomerService.list_customers()
    render(conn, "index.html", customers: customers)
  end

  def new(conn, _params) do
    changeset = Customer.changeset(%Customer{})

    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"customer" => customer_param}) do
    case CustomerService.create_customer(customer_param) do
      {:ok, _} ->
        conn
        |> put_flash(:info, "created successfully")
        |> redirect(to: AsolviWeb.Router.Helpers.item_path(conn, :index))
      _ ->
        conn
        |> put_flash(:error, "failed to create customer")
        |> redirect(to: AsolviWeb.Router.Helpers.item_path(conn, :index))
    end

  end

  def edit(conn, %{"id" => customer_id}) do
    case CustomerService.get_customer(customer_id) do
      nil ->
        redirect(conn, to: AsolviWeb.Router.Helpers.item_path(conn, :index))
      customer ->
        changeset = Customer.update_changeset(customer)

        render(conn, "edit.html", changeset: changeset, customer: customer)
    end
  end

  def update(conn, %{"id" => cid, "customer" => customer_params} = _params) do
    case Asolvi.DynamicWorker.update(cid, customer_params) do
      {:error, _} ->
        conn
        |> put_flash(:error, "could not update")
        |> redirect(to: AsolviWeb.Router.Helpers.item_path(conn, :index))
      {:ok, customer} ->
        conn
        |> put_flash(:info, "customer updated")
        |> render("show.html", customer: customer)
    end
  end

  def show(conn, %{"id" => cid}) do
    case CustomerService.get_customer(cid) do
      nil ->
        conn
        |> put_flash(:error, "customer not found")
        |> redirect(to: AsolviWeb.Router.Helpers.item_path(conn, :index))
      customer ->
        render(conn, "show.html", customer: customer)
    end
  end

  def delete(conn, %{"id" => customer_id}) do
    case Asolvi.DynamicWorker.delete(customer_id) do
      true ->
        conn
        |> put_flash(:info, "deleted successfully")
        |> redirect(to: AsolviWeb.Router.Helpers.item_path(conn, :index))
      _ ->
        conn
        |> put_flash(:error, "failed to delete customer")
        |> redirect(to: AsolviWeb.Router.Helpers.item_path(conn, :index))
    end
  end
end
