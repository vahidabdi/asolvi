defmodule AsolviWeb.ItemControllerTest do
  use AsolviWeb.ConnCase, async: false

  alias Asolvi.CustomerService

  setup do
    Application.stop(:asolvi)
    :ok = Application.start(:asolvi)
    on_exit(fn ->
      File.rm_rf(Application.get_env(:asolvi, :data_path))
    end)
    :ok
  end

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    resp = html_response(conn, 200)

    title =
      resp
      |> Floki.find("h1.title")
      |> Floki.text()
      |> String.trim()

    assert title == "Welcome to Asolvi assignment"
  end

  test "index all customers", %{conn: conn} do
    {:ok, customer1} = CustomerService.create_customer(%{"name" => "name1", "city" => "city1"})
    {:ok, customer2} = CustomerService.create_customer(%{"name" => "name2", "city" => "city2"})
    edit_urls = Enum.map([customer1, customer2], &AsolviWeb.Router.Helpers.item_path(conn, :edit, &1.customer_id))

    conn = get(conn, "/")
    resp = html_response(conn, 200)

    hrefs =
      resp
      |> Floki.find(".column.is-one-third footer a")
      |> Floki.attribute("href")

    assert Enum.all?(edit_urls, &Enum.member?(hrefs, &1))
  end

  test "create customer with valid input", %{conn: conn} do
    conn = post(conn, AsolviWeb.Router.Helpers.item_path(conn, :create), %{"customer" => %{"name" => "Alice", "city" => "stockholm"}})
    assert get_flash(conn, :info) == "created successfully"
  end

  test "create fails with invalid input", %{conn: conn} do
    conn = post(conn, AsolviWeb.Router.Helpers.item_path(conn, :create), %{"customer" => %{"name" => "", "city" => ""}})
    assert get_flash(conn, :error) == "failed to create customer"
  end

  test "update customer with valid input", %{conn: conn} do
    {:ok, customer1} = CustomerService.create_customer(%{"name" => "name1", "city" => "city1"})
    conn = put(conn, AsolviWeb.Router.Helpers.item_path(conn, :update, customer1.customer_id), %{"customer" => %{"name" => "updated_name", "city" => "updated_city"}})

    assert get_flash(conn, :info) == "customer updated"
  end

  test "update fails with invalid input", %{conn: conn} do
    {:ok, customer1} = CustomerService.create_customer(%{"name" => "name1", "city" => "city1"})
    conn = put(conn, AsolviWeb.Router.Helpers.item_path(conn, :update, customer1.customer_id), %{"customer" => %{"name" => "", "city" => ""}})

    assert get_flash(conn, :error) == "could not update"
  end

  test "delete customer", %{conn: conn} do
    {:ok, customer1} = CustomerService.create_customer(%{"name" => "name1", "city" => "city1"})
    conn = delete(conn, AsolviWeb.Router.Helpers.item_path(conn, :delete, customer1.customer_id))

    assert get_flash(conn, :info) == "deleted successfully"
    assert nil == CustomerService.get_customer(customer1.customer_id)
  end
end
