defmodule Asolvi.CustomerStateWorkerTest do
  use ExUnit.Case

  alias Asolvi.CustomerStateWorker
  alias Asolvi.CustomerService

  setup do
    Application.stop(:asolvi)
    :ok = Application.start(:asolvi)
    on_exit(&clean_data_path/0)
    :ok
  end

  test "list_customers will update the list after customer creation" do
    assert map_size(CustomerStateWorker.list_customers()) == 0
    %{customer_id: cid} = create_customer()
    customers = CustomerStateWorker.list_customers()
    assert %{^cid => cs} = customers
  end

  defp create_customer do
    {:ok, customer} = CustomerService.create_customer(%{"name" => "Alice", "city" => "Stockholm"})
    customer
  end

  defp clean_data_path() do
    File.rm_rf(Application.get_env(:asolvi, :data_path))
  end
end
