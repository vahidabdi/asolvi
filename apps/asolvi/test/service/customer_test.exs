defmodule Asolvi.CustomerServiceTest do
  use ExUnit.Case

  alias Asolvi.CustomerService

  @valid_params %{"name" => "Bob", "city" => "Stockholm"}
  @invalid_params %{"name" => "Bob"}

  setup do
    on_exit(fn ->
      File.rm_rf(Application.get_env(:asolvi, :data_path))
    end)
    :ok
  end

  test "create_customer generates valid uuid for customer_id" do
    assert {:ok, %{customer_id: customer_id}} = CustomerService.create_customer(@valid_params)

    assert {:ok, uuid} = Ecto.UUID.cast(customer_id)
  end

  test "create_customers failed with invalid data" do
    assert {:error, changeset} = CustomerService.create_customer(@invalid_params)

    refute changeset.valid?
    assert changeset.errors == [{:city, {"can't be blank", [validation: :required]}}]
  end

  test "update updates customer with valid input" do
    assert {:ok, %{customer_id: cid} = customer} = CustomerService.create_customer(@valid_params)

    {:ok, update_customer} = CustomerService.update_customer(cid, %{"city" => "new city"})
    assert update_customer.city == "new city"
  end

  test "update fails with invalid input" do
    assert {:ok, %{customer_id: cid} = customer} = CustomerService.create_customer(@valid_params)

    {:error, changeset} = CustomerService.update_customer(cid, %{"city" => ""})

    refute changeset.valid?
    assert changeset.errors == [city: {"can't be blank", [validation: :required]}]
  end
end
