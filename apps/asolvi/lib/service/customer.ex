defmodule Asolvi.CustomerService do
  @moduledoc """
  This module is responsible to handle customer `CRUD` operation.

  For `update/delete` it will spawn a unique see `Asolvi.DynamicWorker`
  process for corresponding customer to avoid race condition

  for creation it only updates `Asolvi.CustomerStateWorker` which keeps
  track of persisted customers
  """

  alias Asolvi.Customer

  defdelegate get_customer(cid), to: Asolvi.CustomerStateWorker

  def list_customers() do
    Asolvi.CustomerStateWorker.list_customers
    |> Map.values()
  end

  def create_customer(params \\ %{}) do
    with {:ok, customer} <- transform(params) do
      Asolvi.Utils.File.create_customer_file(customer)
      customer = Asolvi.CustomerStateWorker.create_customer(customer)
      {:ok, customer}
    else
      {:error, changeset} -> {:error, changeset}
      nil -> {:error, :not_found}
    end
  end

  def update_customer(cid, params) do
    customer = get_customer(cid)
    with {:ok, updated_customer} <- update_transform(customer, params) do
      Asolvi.Utils.File.update_customer_file(updated_customer)
      customer = Asolvi.CustomerStateWorker.update_customer(updated_customer)
      {:ok, customer}
    else
      {:error, changeset} -> {:error, changeset}
      nil -> {:error, :not_found}
    end
  end

  def delete_customer(cid) do
    case get_customer(cid) do
      nil ->
        false
      customer ->
        Asolvi.Utils.File.delete_customer_file(customer)
        Asolvi.CustomerStateWorker.delete_customer(customer)
        true
    end
  end

  defp transform(params) do
    params =
      params
      |> Map.put("customer_id", Ecto.UUID.generate)
      |> Map.put("inserted_at", Ecto.Schema.__timestamps__(:naive_datetime))
      |> Map.put("updated_at", Ecto.Schema.__timestamps__(:naive_datetime))

    case Customer.changeset(%Customer{}, params) do
      changeset = %{valid?: true} ->
        {:ok, Ecto.Changeset.apply_changes(changeset)}
      changeset ->
        {:error, changeset}
    end
  end
  defp update_transform(customer, params) do
    params =
      params
      |> Map.put("updated_at", Ecto.Schema.__timestamps__(:naive_datetime))

    case Customer.update_changeset(customer, params) do
      changeset = %{valid?: true} ->
        {:ok, Ecto.Changeset.apply_changes(changeset)}
      changeset ->
        {:error, changeset}
    end
  end
end
