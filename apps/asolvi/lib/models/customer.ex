defmodule Asolvi.Customer do
  use Ecto.Schema

  import Ecto.Changeset

  @customer_fields ~w(customer_id name city address phone_number inserted_at updated_at)a
  @customer_required_fields ~w(customer_id name city)a

  @primary_key false
  embedded_schema do
    field(:customer_id, :binary_id)
    field(:name, :string)
    field(:city, :string)
    field(:address, :string)
    field(:phone_number, :string)

    timestamps()
  end

  def changeset(%__MODULE__{} = model, params \\ %{}) do
    model
    |> cast(params, @customer_fields)
    |> validate_required(@customer_required_fields)
  end

  def update_changeset(%__MODULE__{} = model, params \\ %{}) do
    model
    |> cast(params, @customer_fields -- ~w(customer_id inserted_at)a)
    |> validate_required(@customer_required_fields -- ~w(customer_id)a)
  end
end
