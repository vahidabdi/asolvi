defmodule Asolvi.DynamicWorker do
  use GenServer

  def start_link(name) do
    GenServer.start_link(__MODULE__, name, name: via_tuple(name))
  end

  def init(name) do
    {:ok, name}
  end

  def update(cid, params) do
    Asolvi.WorkerSupervisor.start_child(cid)
    GenServer.call(via_tuple(cid), {:update, cid, params})
  end

  def delete(cid) do
    Asolvi.WorkerSupervisor.start_child(cid)
    GenServer.call(via_tuple(cid), {:delete, cid})
  end

  # Callbacks
  def handle_call({:update, cid, params}, _from, state) do
    case Asolvi.CustomerService.update_customer(cid, params) do
      {:ok, update_customer} ->
        {:reply, {:ok, update_customer}, state}
      error ->
        {:reply, error, state}
    end
  end
  def handle_call({:delete, cid}, _from, state) do
    res = Asolvi.CustomerService.delete_customer(cid)
    {:reply, res, state}
  end

  defp via_tuple(name) do
    {:via, Registry, {:customer_registry, name}}
  end
end
