defmodule Asolvi.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    import Supervisor.Spec

    children = [
      # :poolboy.child_spec(:customer_worker, customer_worker_pool_config()), # Use for customer creation
      worker(Asolvi.CustomerStateWorker, []), # Maintain list of customers
      { Asolvi.WorkerSupervisor, [] }, # Dynamically create unique process per customer
      { Registry, [keys: :unique, name: :customer_registry] }
    ]

    opts = [strategy: :one_for_one, name: Asolvi.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # defp customer_worker_pool_config() do
  #   [
  #     {:name, {:local, :customer_worker}},
  #     {:worker_module, Asolvi.CustomerWorker},
  #     {:size, 5},
  #     {:max_overflow, 2},
  #   ]
  # end
end
