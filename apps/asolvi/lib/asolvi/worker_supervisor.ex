defmodule Asolvi.WorkerSupervisor do
  use DynamicSupervisor
  alias Asolvi.DynamicWorker

  def start_link(_) do
    DynamicSupervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_args) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  def start_child(child_name) do
    DynamicSupervisor.start_child(
      __MODULE__,
      %{id: DynamicWorker, start: { DynamicWorker, :start_link,  [child_name]}, restart: :transient}
    )
  end
end
