defmodule Asolvi.CustomerStateWorker do

  @data_path Application.get_env(:asolvi, :data_path)

  def start_link do
    Agent.start_link(
      fn ->
        unless(File.dir?(@data_path), do: File.mkdir_p!(@data_path))
        load_data_from(@data_path)
      end,
      name: __MODULE__
    )
  end

  def list_customers() do
    Agent.get(__MODULE__, &(&1))
  end

  def get_customer(customer_id) do
    Agent.get(__MODULE__, fn state ->
      Map.get(state, customer_id)
    end)
  end

  def create_customer(%{customer_id: customer_id} = customer) do
    Agent.get_and_update(__MODULE__, fn state ->
      new_state = Map.put_new(state, customer_id, customer)
      {customer, new_state}
    end)
  end

  def update_customer(%{customer_id: cid} = customer) do
    Agent.get_and_update(__MODULE__, fn state ->
      new_state = Map.put(state, cid, customer)
      {customer, new_state}
    end)
  end

  def delete_customer(%{customer_id: cid}) do
    Agent.update(__MODULE__, fn state ->
      Map.delete(state, cid)
    end)
  end

  def refresh_customers_list() do
    Agent.update(__MODULE__, fn _ -> load_data_from(@data_path) end)
  end

  def put(%{customer_id: customer_id} = customer) do
    Agent.update(__MODULE__, &Map.put(&1, customer_id, customer))
  end

  defp load_data_from(path) do
    {:ok, files} = File.ls(path)

    files
    |> Enum.filter(&String.ends_with?(&1, ".erl_term"))
    |> Enum.map(&(load_customer(&1, path)))
    |> Enum.into(%{})
  end

  defp load_customer(file, path) do
    uuid = binary_part(file, 0, 36) # extract uuid part
    content = File.read!(Path.join(path, file))
    {uuid, :erlang.binary_to_term(content)}
  end
end
