defmodule Asolvi.Utils.File do

  @data_path Application.get_env(:asolvi, :data_path)

  def create_customer_file(customer) do
    content = :erlang.term_to_binary(customer)
    File.write(customer_path(customer), content)
  end
  def update_customer_file(customer), do: create_customer_file(customer)

  def delete_customer_file(customer) do
    File.rm(customer_path(customer))
  end

  def customer_path(%{customer_id: cid}), do: @data_path <> "/#{cid}.erl_term"
end
